<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="histoire")
 */
class Histoire {

    const DIR_UPLOAD = 'histoire';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $titre;

    /**
     * @ORM\Column(type="text", length=500)
     */
    private $pitch;

    /**
     *
     * @Assert\File(mimeTypes={"image/png", "image/jpeg", "image/gif"})
     */
    private $photoFile;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Genre", inversedBy="histoires")
     */
    private $genre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="histoires")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chapitre", mappedBy="histoire")
     */
    private $chapitres;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="histoireId")
     */
    private $avis;

    /**
     * @return mixed
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre): void {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getPitch() {
        return $this->pitch;
    }

    /**
     * @param mixed $pitch
     */
    public function setPitch($pitch): void {
        $this->pitch = $pitch;
    }

    /**
     * @return mixed
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void {
        $this->photo = $photo;
    }

    /**
     * @return mixed
     */
    public function getGenre() {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre): void {
        $this->genre = $genre;
    }


    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getActif() {
        return $this->actif;
    }

    /**
     * @param mixed $actif
     */
    public function setActif($actif): void {
        $this->actif = $actif;
    }

    /**
     * @return mixed
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    /**
     * @param mixed $avis
     */
    public function setAvis($avis)
    {
        $this->avis = $avis;
    }

    /**
     * @return mixed
     */
    public function getChapitres()
    {
        return $this->chapitres;
    }

    /**
     * @param mixed $chapitres
     */
    public function setChapitres($chapitres)
    {
        $this->chapitres = $chapitres;
    }

    /**
     * @return mixed
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    /**
     * @param mixed $photoFile
     * @return Histoire
     */
    public function setPhotoFile($photoFile)
    {
        $this->photoFile = $photoFile;
        return $this;
    }



}