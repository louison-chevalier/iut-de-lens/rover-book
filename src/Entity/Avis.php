<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisRepository")
 */
class Avis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $positif;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Histoire", inversedBy="avis")
     */
    private $histoire;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="avis")
     */
    private $user;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPositif(): ?bool
    {
        return $this->positif;
    }

    public function setPositif(bool $positif): self
    {
        $this->positif = $positif;

        return $this;
    }

    /**
     * @return Histoire
     */
    public function getHistoire(): Histoire
    {
        return $this->histoire;
    }

    /**
     * @param mixed $histoire
     */
    public function setHistoire($histoire)
    {
        $this->histoire = $histoire;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}
