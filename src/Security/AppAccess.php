<?php

namespace App\Security;

final class AppAccess{

    const HISTOIRE_VOIR = 'histoire.voir';
    const HISTOIRE_EDITER = 'histoire.editer';
    const HISTOIRE_SUPPRIMER = 'histoire.supprimer';
    const CHAPITRE_VOIR = 'chapitre.voir';
    const CHAPITRE_EDITER = 'chapitre.editer';
    const CHAPITRE_SUPPRIMER = 'chapitre.supprimer';
}