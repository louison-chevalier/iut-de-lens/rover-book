<?php

namespace App\Security\Voter;

use App\Entity\Histoire;
use App\Entity\User;
use App\Security\AppAccess;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class HistoireVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
                AppAccess::HISTOIRE_EDITER,
                AppAccess::HISTOIRE_SUPPRIMER])
        ) {
            return false;
        }

        if (!$subject instanceof Histoire) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {

            return false;
        }

        if ($this->security->isGranted('ROLE_ADMIN') === true) {
            return true;
        }

        return $subject->getUser()->getId() === $user->getId(); // donc uniquement admin & créateur
    }
}
