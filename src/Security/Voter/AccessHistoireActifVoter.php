<?php

namespace App\Security\Voter;

use App\Entity\Histoire;
use App\Entity\User;
use App\Security\AppAccess;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AccessHistoireActifVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        if ($attribute === AppAccess::CHAPITRE_VOIR) {
            return false;
        }

        if (!$subject instanceof Histoire) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN') === true) {
            return true;
        }

        if($subject->getActif() === false)
        {
            return false;
        }

        return true;  // donc uniquement admin & créateur
    }
}
