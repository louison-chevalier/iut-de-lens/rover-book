<?php
/**
 * Created by PhpStorm.
 * User: sandra.albert
 * Date: 19/12/18
 * Time: 22:48
 */

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;

/**
 * @Route("/auteur")
 */
class AuteurController extends AbstractController
{
    /**
     * @Route("/{id}", name="auteur_show", methods="GET")
     */
    public function show(User $user): Response
    {
        return $this->render('user/histoires.html.twig', ['user'=>$user]);
    }
}