<?php

namespace App\Controller;

use App\Entity\Histoire;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VisualisationController extends AbstractController
{
    /**
     * @Route("/", name="visualisation")
     */
    public function index()
    {
		$histoires = $this->getDoctrine()
			->getRepository(Histoire::class)
			->findAll();

		return $this->render('visualisation/index.html.twig', ['histoires' => $histoires]);
    }

	/**
	 * @Route("/compte_rendu", name="compte_rendu")
	 */
    public function compte_rendu()
	{
		$histoiresRepository = $this->getDoctrine()
			->getRepository(Histoire::class);

		$histoires = $histoiresRepository->findAll();

		return $this->render('visualisation/compte_rendu.html.twig', ['histoires' => $histoires]);
	}
}
