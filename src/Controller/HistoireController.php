<?php

namespace App\Controller;

use App\Entity\Chapitre;
use App\Entity\Histoire;
use App\Entity\Avis;
use App\Entity\Lecture;
use App\Form\HistoireType;
use App\Repository\AvisRepository;
use App\Security\AppAccess;
use App\Upload\FileHistoireUpload;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @Route("/histoire")
 */
class HistoireController extends AbstractController
{
    /**
     * @Route("/", name="histoire_index", methods="GET")
     */
    public function index(): Response
    {
        $histoires = $this->getDoctrine()
            ->getRepository(Histoire::class)
            ->findAll();

        return $this->render('histoire/index.html.twig', ['histoires' => $histoires]);
    }

    /**
     * @Route("/{id}", name="histoire_show", methods="GET")
     */
    public function show(Histoire $histoire,AuthorizationCheckerInterface $securityChecker): Response
    {
        $this->denyAccessUnlessGranted(AppAccess::HISTOIRE_VOIR, $histoire);
        $premierChapitre = $this->getDoctrine()
            ->getRepository(Chapitre::class)
            ->findOneBy(['histoire' => $histoire, 'premier' => 1]);
        $avisPositifs = $this->getDoctrine()
            ->getRepository(Avis::class)
            ->findBy(['histoire' => $histoire, 'positif' => 1]);
        $avisNegatifs = $this->getDoctrine()
            ->getRepository(Avis::class)
            ->findBy(['histoire' => $histoire, 'positif' => 0]);

        $avancement = null;
        if ($securityChecker->isGranted('ROLE_USER')) {
            $avancement = $this->getDoctrine()
                ->getRepository(Lecture::class)->findOneBy([
                'histoire' => $histoire,
                'user' => $this->getUser()], ['numSequence' => 'DESC']);//
        }
        return $this->render('histoire/show.html.twig', [
            'histoire' => $histoire,
            'premier' => $premierChapitre,
            'positifs' => $avisPositifs,
            'negatifs' => $avisNegatifs,
            'avancement' => $avancement
        ]);
    }

    /**
     * @Route("/{id}/edit", name="histoire_edit", methods="GET|POST")
     */
    public function edit(Request $request, Histoire $histoire, FileHistoireUpload $fileHistoireUpload): Response
    {
        $this->denyAccessUnlessGranted(AppAccess::HISTOIRE_EDITER, $histoire);
        $form = $this->createForm(HistoireType::class, $histoire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileHistoireUpload->upload($histoire);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('histoire_index', ['id' => $histoire->getId()]);
        }

        return $this->render('histoire/edit.html.twig', [
            'histoire' => $histoire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="histoire_delete", methods="DELETE")
     */
    public function delete(Request $request, Histoire $histoire,FileHistoireUpload $fileHistoireUpload): Response
    {
        $this->denyAccessUnlessGranted(AppAccess::HISTOIRE_SUPPRIMER, $histoire);
        if ($this->isCsrfTokenValid('delete' . $histoire->getId(), $request->request->get('_token'))) {
            if($fileHistoireUpload->removeFile($histoire) === true)
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($histoire);
                $em->flush();
            }
        }

        return $this->redirectToRoute('histoire_index');
    }

    /**
     * @Route("/{id}/like", name="histoire_like")
     */
    public function like(Histoire $histoire, AvisRepository $ar){
        $user = $this->getUser();

        if($user === null){
            return $this->redirectToRoute('app_login');
        }

        $em = $this->getDoctrine()->getManager();
        $like = $ar->findOneBy(['histoire' => $histoire, 'user' => $user]);

        if ($like !== null){
            $em->remove($like);
            $em->flush();
        }
        $like = new Avis();
        $like->setHistoire($histoire);
        $like->setUser($user);
        $like->setPositif(1);
        $em->persist($like);
        $em->flush();
        return $this->redirectToRoute('histoire_show', ['id'=>$histoire->getId()]);
    }

    /**
     * @Route("/{id}/dislike", name="histoire_dislike")
     */
    public function dislike(Histoire $histoire, AvisRepository $ar){
        $user = $this->getUser();

        if($user === null){
            return $this->redirectToRoute('app_login');
        }

        $em = $this->getDoctrine()->getManager();
        $dislike = $ar->findOneBy(['histoire' => $histoire, 'user' => $user]);

        if ($dislike !== null){
            $em->remove($dislike);
            $em->flush();
        }
        $dislike = new Avis();
        $dislike->setHistoire($histoire);
        $dislike->setUser($user);
        $dislike->setPositif(0);
        $em->persist($dislike);
        $em->flush();
        return $this->redirectToRoute('histoire_show', ['id'=>$histoire->getId()]);
    }
}
