<?php

namespace App\Controller;

use App\Entity\Avis;
use App\Entity\Chapitre;
use App\Entity\Lecture;
use App\Form\ChapitreType;
use App\Repository\ChapitreRepository;
use App\Repository\LectureRepository;
use App\Security\AppAccess;
use App\Upload\FileChapitreUpload;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @Route("/chapitre")
 */
class ChapitreController extends AbstractController
{
    /**
     * @Route("/", name="chapitre_index", methods="GET")
     */
    public function index(ChapitreRepository $chapitreRepository): Response
    {
        return $this->render('chapitre/index.html.twig', ['chapitres' => $chapitreRepository->findAll()]);
    }

    /**
     * @Route("/{id}", name="chapitre_show", methods="GET")
     */
    public function show(Chapitre $chapitre, AuthorizationCheckerInterface $securityChecker, LectureRepository $lectureRepo): Response
    {
        $filAriane = new ArrayCollection();
        $chapitreAux = $chapitre;
        while (!$chapitreAux->isPremier()) {
            $filAriane[] = ['id' => $chapitreAux->getId(), 'titre' => $chapitreAux->getTitreCourt()];
            $chapitreAux = $chapitreAux->getOrigines()->first()->getChapitreSource();
        }
        $filAriane[] = ['id' => $chapitreAux->getId(), 'titre' => $chapitreAux->getTitreCourt()];

        $em = $this->getDoctrine()->getManager();
        if ($securityChecker->isGranted('ROLE_USER')) {
            /*
             * Cherchez si il y a une ligne avec le chapitre source (et l'utilisateur et l'histoire) dans la base.
             * Si oui, détruisez toutes les lignes (avec l'utilisateur et l'histoire) et le numero de séquence supérieur
             * à celui récupéré). Le prochaine numéro de séquence sera égal à celui récupéré + 1
             * Si non, c'est le premier chapitre et le numéro de séquence vaut 1.
            */
            $laLigne = $lectureRepo->findOneBy([
                'histoire' => $chapitre->getHistoire(),
                'user' => $this->getUser()], ['numSequence' => 'DESC']);
            $numSeq = 0;
            if ($laLigne !== null) // oui
            {
                if ($chapitre->isPremier() === false) {
                    $numSeq = $laLigne->getNumSequence();
                }

                $lesLignes = $lectureRepo->findAllGreaterThanNumSequence($this->getUser(), $chapitre->getHistoire(), $numSeq);
                foreach ($lesLignes as $ligne) {
                    $em->remove($ligne);
                }
                $em->flush();

            }

            $lecture = new Lecture();
            $lecture->setUser($this->getUser())
                ->setChapitre($chapitre)
                ->setHistoire($chapitre->getHistoire())
                ->setNumSequence($numSeq + 1)
                ->setDateLecture(new \DateTime());

            $em->persist($lecture);
            $em->flush();
        }
        $avisPositifs = $this->getDoctrine()
            ->getRepository(Avis::class)
            ->findBy(['histoire' => $chapitre->getHistoire(), 'positif' => 1]);
        $avisNegatifs = $this->getDoctrine()
            ->getRepository(Avis::class)
            ->findBy(['histoire' => $chapitre->getHistoire(), 'positif' => 0]);

        return $this->render('chapitre/show.html.twig', ['chapitre' => $chapitre,
            'fil' => $filAriane,
            'positifs' => $avisPositifs,
            'negatifs' => $avisNegatifs]);
    }

    /**
     * @Route("/{id}/edit", name="chapitre_edit", methods="GET|POST")
     */
    public function edit(Request $request, Chapitre $chapitre, FileChapitreUpload $fileChapitreUpload): Response
    {
        $this->denyAccessUnlessGranted(AppAccess::CHAPITRE_EDITER, $chapitre);
        $form = $this->createForm(ChapitreType::class, $chapitre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileChapitreUpload->upload($chapitre);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chapitre_index', ['id' => $chapitre->getId()]);
        }

        return $this->render('chapitre/edit.html.twig', [
            'chapitre' => $chapitre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="chapitre_delete", methods="DELETE")
     */
    public function delete(Request $request, Chapitre $chapitre, FileChapitreUpload $fileChapitreUpload): Response
    {
        $this->denyAccessUnlessGranted(AppAccess::CHAPITRE_SUPPRIMER, $chapitre);
        if ($this->isCsrfTokenValid('delete' . $chapitre->getId(), $request->request->get('_token'))) {
            if ($fileChapitreUpload->removeFile($chapitre) === true) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($chapitre);
                $em->flush();
            }
        }

        return $this->redirectToRoute('chapitre_index');
    }
}
