<?php

namespace App\Controller;

use App\Entity\Suite;
use App\Repository\HistoireRepository;
use App\Upload\FileChapitreUpload;
use App\Upload\FileHistoireUpload;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Histoire;
use App\Form\HistoireType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Chapitre;
use App\Form\ChapitreType;

class CreationController extends AbstractController
{
    /**
     * @Route("/creation", name="creation")
     */
    public function index()
    {
        return $this->render('creation/index.html.twig', [
            'controller_name' => 'CreationController',
        ]);
    }


    //---------------------------------------------------------

    /**
     * @Route("/creation/histoire", name="creer_histoire", methods="GET|POST")
     */
    public function creerHistoire(Request $request, FileHistoireUpload $fileHistoireUpload): Response
    {
        $histoire = new Histoire();
        $form = $this->createForm(HistoireType::class, $histoire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileHistoireUpload->upload($histoire);
            $this->enregistrerHistoire($histoire);
            return $this->redirectToRoute('histoire_index');
        }

        return $this->render('histoire/new.html.twig', [
            'histoire' => $histoire,
            'form' => $form->createView(),
        ]);
    }

    public function enregistrerHistoire(Histoire $histoire)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($histoire);
        $em->flush();
    }


    //---------------------------------------------------------

    /**
     * @Route("/creation/chapitre", name="creer_chapitre", methods="GET|POST")
     */
    public function creerChapitre(Request $request, FileChapitreUpload $fileChapitreUpload): Response
    {
        $chapitre = new Chapitre();
        $form = $this->createForm(ChapitreType::class, $chapitre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fileChapitreUpload->upload($chapitre);
            if (count($chapitre->getHistoire()->getChapitres()) === 0) {
                $chapitre->setPremier(true);
            }

            $this->enregistrerChapitre($chapitre);
            return $this->redirectToRoute('chapitre_index');
        }

        return $this->render('chapitre/new.html.twig', [
            'chapitre' => $chapitre,
            'form' => $form->createView(),
        ]);
    }

    public function enregistrerChapitre(Chapitre $chapitre)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($chapitre);
        $em->flush();
    }


    //---------------------------------------------------------

    /**
     * @Route("/creation/suite", name="lier_chapitre")
     */
    public function lierChapitre(Request $request, HistoireRepository $repoHistoire)
    {
        $lesHistoires = $repoHistoire->findBy(['user' => $this->getUser()]); // étape 1
        $form = $this->createFormBuilder()
            ->add('histoire', EntityType::class, array(
                'class' => Histoire::class,
                'choice_label' => 'titre',
                'choices' => $lesHistoires))
            ->add('save', SubmitType::class, array('label' => 'Gerer les chapitres'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $histoireChoisi = $form->getData();

            return $this->redirectToRoute('lier_chapitre_question', ['histoire' => $histoireChoisi['histoire']->getId()]);
        }

        return $this->render('creation/lier_chapitre.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/creation/suite/{histoire}", name="lier_chapitre_question")
     */
    public function lierChapitreQuestion(Histoire $histoire, Request $request)
    {
        $lesChapitres = $histoire->getChapitres();

        $suite = new Suite();

        $form = $this->createFormBuilder($suite)
            ->add('reponse', TextType::class)
            ->add('chapitreSource', EntityType::class,
                [
                    'class' => Chapitre::class,
                    'choice_label' => 'titre',
                    'choices' => $lesChapitres
                ])
            ->add('chapitreDestination', EntityType::class,
                [
                    'class' => Chapitre::class,
                    'choice_label' => 'titre',
                    'choices' => $lesChapitres
                ])
            ->add('save', SubmitType::class, array('label' => 'Gerer les questions'))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($suite);
            $em->flush();

            return $this->redirectToRoute('lier_chapitre_question',
                ['histoire' => $histoire->getId()]);
        }


        return $this->render('creation/lier_chapitre.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    public function enregistrerLiaison()
    {
        // TODO
    }


}
