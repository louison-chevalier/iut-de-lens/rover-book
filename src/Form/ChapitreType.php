<?php

namespace App\Form;

use App\Entity\Chapitre;
use App\Entity\Histoire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChapitreType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class)
            ->add('texte', TextareaType::class)
            ->add('titreCourt', TextType::class)
            ->add('photoFile', FileType::class,array(
                'required' => false
            ))
            ->add('question', TextType::class, array(
                'required' => false
            ))
            ->add('histoire', EntityType::class, array(
                'class' => Histoire::class,
                'choice_label' => 'titre'))
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            array($this, 'preSetData')
        );
    }

    public function preSetData(FormEvent $event)
    {
        $item = $event->getData();

        if($item->getId() === null){
            $item->setPremier(false);// si on crée l'histoire, le premier est false de base
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Chapitre::class
        ]);
    }
}
