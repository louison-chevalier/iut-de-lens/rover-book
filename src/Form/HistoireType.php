<?php

namespace App\Form;

use App\Entity\Genre;
use App\Entity\Histoire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class HistoireType extends AbstractType
{

    private $securityChecker;
    private $token;

    public function __construct(AuthorizationCheckerInterface $securityChecker, TokenStorageInterface $token)
    {
        $this->securityChecker = $securityChecker;
        $this->token = $token;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class)
            ->add('pitch', TextareaType::class)
            ->add('photoFile', FileType::class,array(
                'required' => false
            ))
            ->add('genre', EntityType::class, array(
                'class' => Genre::class,
                'choice_label' => 'label'))
            ->add('user', null, ['choice_label' => 'email', 'placeholder' => false]);
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            array($this, 'preSetData')
        );
    }

    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $item = $event->getData();

        $item->setUser($this->token->getToken()->getUser());
        $form->remove('user');

        if ($item->getId() === null) {//si on le crée, il sera en false de base, sinon il y aura un champ
            $item->setActif(false);

        } else {
            $form->add('actif', ChoiceType::class, array(
                'choices' => array(
                    'Actif' => true,
                    'Inactif' => false
                )
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Histoire::class,
        ]);
    }
}
